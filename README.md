#CarCar

Team:

* Joshua Covell - Services Microservice
* Matthew Kane - Sales Microservice

## Getting Started

**Make sure you have docker installed

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<repository.url.here>>

3. Build and run the project using Docker with these commands in your terminal:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all docker containers are running
- Once the React container has finished compiling (can take up to a few minutes), you can view the project in your browser: http://localhost:3000/

## Design

CarCar is made up of 3 microservices, a database, and a front end React library that interact with eachother to create an interactive single page application. CarCar
is modeled following principles of Domain Driven Design and contains entities and value objects. The microservices are in a RESTful API architecture.

### Inventory Microservice
The Inventory Microservice manages the creation, deletion, and updates of vehicle manufacturers, models, and automobiles in the inventory.

    - Ports: 8100:8000
    - URL Paths and Crud Operations:


| Operation            | URL Path                | Description                                                     |
|----------------------|-------------------------|-----------------------------------------------------------------|
|                      | `api/`                  | Directs to inventory application URLs.                          |
|`GET`, `POST`,        | `automobiles/`          | Lists all automobiles in database or creates new automobile.    |
|`GET`, `DELETE`, `PUT`| `automobiles/<str:vin>` | Show, Delete, and update automobile with specific VIN.          |
|`GET`,`POST`,         | `manufacturers/`        | Lists all manufacturers in database or creates new manufacturer.|
|`GET`, `DELETE`, `PUT`| `manufacturers/<int:pk>`| Show, Delete, and update manufacturer with specific ID.         |
|`GET`,`POST`,         | `models/`               | Lists all models in database or creates new model.              |
|`GET`, `DELETE`, `PUT`| `models/<int:pk>`       | Show, Delete, and update model with specific ID.                |

-Json Data Bodies:

Create a manufacturer:
```
{
    "name": "manufacturer name"
}
```

Update a manufacturer:
```
{
    "name": "manufacturer name"
}
```

Create a model:
```
{
    "name": "Model name",
    "picture_url": "URL of picture",
    "manufacturer_id": <int>Manufacturer ID
}
```

Update a model:
```
{
    "name": "Model name",
    "picture_url": "URL of picture",
    "manufacturer_id": <int>Manufacturer ID
}
```

Create an automobile("sold" defaults to false)("vin" is a max_length 17 character string value that is unique to the vehicle):
```
{
    "color": "color",
    "year": <int>year,
    "vin": "vin number",
    "model_id": <int>Model id
}
```

Update an automobile:
```
{
    "color": "color",
    "year": <int>year,
    "vin": "vin number",
    "model_id": <int>Model id
    "sold": lowercase boolean
}
```

### Services Microservice
The Services Microservice manages the creation, deletion, and updates to employed technicians and service appointments. It keeps track of upcoming appointments and a history of past appointments with details of the customer, appointment date/time, VIP status (determined if the VIN is from a vehicle sold from the inventory), the reason for the appointment, the technician assigned to the service, and the status of the appointment (incomplete, complete, canceled).

    - Ports: 8080:8000
    - URL Paths and Crud Operations:


| Operation            | URL Path                             | Description                                                     |
|----------------------|--------------------------------------|-----------------------------------------------------------------|
|                      | `http://localhost:8080/api/`         | Directs to inventory application URLs.                          |
|`GET`, `POST`         | `technicians/`                       | Lists all technicians in database or creates new technician.    |
|`DELETE`              | `technicians/<int:pk>`               | Delete technician with specific ID.                             |
|`GET`,`POST`,         | `appointments/`                      | Lists all appointments in database or creates new appointment.  |
|`DELETE`              | `appointments/<int:pk>/`             | Delete an appointment with specific ID.                         |
|`PUT`                 | `appointments/<int:pk>/cancel/`      | Set status of appointment with specific ID to "Canceled"        |
|`PUT`                 | `appointments/<int:pk>/finish/`      | Set status of appointment with specific ID to "Completed"       |


TECHNICIAN APIs:

To get a list of all technicians, send a POST request to the URL Path described above. See example below for return result (the "id" field is automatically generated):
```
{
	"technicians": [
		{
			"id": 1,
			"first_name": "Tech",
			"last_name": "Nician",
			"employee_id": "123"
            "appointments": 2
		},
        {
			"id": 2,
			"first_name": "John",
			"last_name": "Doe",
			"employee_id": "456"
            "appointments": 3
		}
	]
}
```

To create a new technician, you will need to send a POST request to the URL path described above using the format below in the body of the JSON:
```
{
	"first_name": "Jason",
    "last_name": "Bourne",
	"employee_id": 789
}
```

To Delete a technician, send a DELETE request to the URL path described above, using the ID to identify the specific technician to delete (ID can be found in the list of technicians). If successful, the API should return the following:
```
{
	"deleted": true
}
```

APPOINTMENT APIs:
To get a list of all appointments, send a POST request to the URL Path described above. See example below for return result (the "id" field is automatically generated):
```
{
	"appointments": [
		{
            "id": 1,
            "vip": false,
            "date_time": "2023-12-19 15:30",
            "reason": "oil change",
            "status": "Incomplete",
            "vin": "1234567890asdfghj",
            "customer": "Jane Doe",
            "technician": "Tech Nician"
        },
        {
			"id": 2,
			"first_name": "John",
			"last_name": "Doe",
			"employee_id": "456"
		}
	]
}
```

To create a new appointment, you will need to send a POST request to the URL path described above using the format below to input in the body of the JSON (The "technician" field must match an existing technician's employee ID from the list of technicians above. "date_time" for the appointment is in local time and must match the format below):
```
{
    "date_time": "2023-12-19 15:30",
    "reason": "new tires",
    "vin": "asdfghj1234567890",
    "customer": "Joe Shmoe",
	"technician": 12345
}
```

To Delete an appointment, send a DELETE request to the URL path described above, using the ID to identify the specific appointment to delete (ID can be found in the list of appointments). If successful, the API should return the following:
```
{
	"deleted": true
}
```

To Update the status of an appointment, send a PUT request to the URL path described above, using the ID to identify the specific appointment to update and either "cancel" or "finish" as the status (ID can be found in the list of appointments). Example for a PUT request to cancel appointment with an id of 1 below (http://localhost8080:api/appointments/1/cancel/):
```
{
	"id": 1,
	"vip": false,
	"date_time": "2023-12-19T04:23:00+00:00",
	"reason": "new tires",
	"status": "Canceled",
	"vin": "12345asdfg",
	"customer": "Jane Doe",
	"technician": "Tech Nician"
}
```

SERVICES MODELS:

The Service Microservice contains the following enity and value object models:

    1. Technician
        Properties:
            - first_name
            - last_name
            - employee_id (must be unique)

    2. Appointment
        Properties:
            - date_time (date/time of appointment)
            - reason (reason for appointment)
            - status (default is incomplete when first created. Can be updated to "complete" or "canceled")
            - vin (vehicle VIN, must be 17 or less characters)
            - customer
            - technician (many-to-one relationship with Technician model)
            - vip (true/false, backend determines if customer is VIP if VIN for appointment matches a VIN in inventory)

    3. AutomobileVO (Automobile Value Objects are created/updated using by polling from inventory microservice Automobile entity objects every minute)
        Properties (all polled from inventory microservice):
            - vin
            - sold (true/false)
            - import_href

### Sales Microservice
Sales microservice allows for the creation and deletion of data related to customers, salespeople, sales records, and the sold field in the automobile record.
The sales microservice houses the entities customers, salespeople and sales and the value object of the automobile entity of the inventory microservice.

    - Ports: 8090:8000
    - URL Paths and Crud Operations:


| Operation     | URL Path                                   |Description                         |
|---------------|--------------------------------------------|------------------------------------|
|               |'http://localhost:8090/api/'                |Directs to sales application URLs.  |
|`GET`, `POST`  |'http://localhost:8090/salespeople/'        |Lists all salespeople in database.  |
|`GET`, `DELETE`|'http://localhost:8090/salespeople/<int:id>'|Gets or deletes a salesperson.      |
|`GET`, `POST`  |'http://localhost:8090/customers/'          |Lists all customers in database.    |
|`GET`, `DELETE`|'http://localhost:8090/customers/<int:id>'  |Gets or deletes a customer.         |
|`GET`, `POST`  |'http://localhost:8090/sales/'              |Lists all sales records in database.|
|`GET`, `DELETE`|'http://localhost:8090/sales/<int:id>'      |Gets or deletes a sales record.     |

-Json Data Bodies:

Create a salesperson:
```
{
    "name": "New Vehicle Model",
    "picture_url": "picture URL",
    "manufacturer_id": <int>manufacturer id
}
```

Create a customer:
```
{
    "first_name": "First name",
    "last_name": "Last name",
    "address": "Address",
    "phone_number": "xxx-xxx-xxxx"
}
```
Create a sales record:
```
{
    "price": "<int>price",
    "salesperson": <int>employee_id,
    "customer": "9",
    "automobile": "automobile vin #"
}
```

SALES MODELS:

    1. Customer model is independent of other models

        Customer properties:
            -first_name
            -last_name
            -address
            -phone_number (unique)

    2. Saleperson model is independent of other models

        Salesperson properties:
            -first_name
            -last_name
            -empoloyee_id (unique)

    3. Sale model needs a Customer, Salesperson, and AutomobileVO instance to be created. When a sale is created it will update
        AutomobileVO and Automobile 'sold' field to true. When the sale is deleted it will
        update AutomobileVO and Automobile 'sold' field to false. Deleting a model from the inventory will
        remove all Automobile instances but not AutomobileVO instances, therefore there may be vins in the sales
        list but not in the automobiles available for sale when that sale is deleted.

        Sale properties:
            -price
            -salesperson (foreign key to Salesperson)
            -customer (foreign key to Customer)
            -automobile (foreign key to AutomobileVO)

    4. AutomobileVO is a Value object that uses a poller every 60 seconds to inventory API database in order to acquire new instances

        AutomobileVO properties:
            -vin
            -sold

### React Front End
The React front end provides the user an interactive Single Page Application that allows the user to interact with each of the microservices.

    - Ports: 3000:3000
    - URL Paths for React Front End:


| URL                                        | Description                                                                    |
|--------------------------------------------|--------------------------------------------------------------------------------|
|'http://localhost:3000/'                    |Main Page                                                                       |
|'http://localhost:3000/manufacturers'       |Manufacturers List                                                              |
|'http://localhost:3000/manufacturers/new'   |Manufacturers Form                                                              |
|'http://localhost:3000/automobiles'         |Automobiles List filtered to show only vehicles with "sold":false               |
|'http://localhost:3000/automobiles/new'     |Automobiles Form filtered to show form once a model is available                |
|'http://localhost:3000/models'              |Vehicle Model List                                                              |
|'http://localhost:3000/models/new'          |Vehicle Model Form                                                              |
|'http://localhost:3000/technicians'         |Technicians List                                                                |
|'http://localhost:3000/technicians/new'     |Technicians Form                                                                |
|'http://localhost:3000/appointments'        |Appointments List                                                               |
|'http://localhost:3000/appointments/new'    |Appointments Form                                                               |
|'http://localhost:3000/appointments/history'|Appointments history                                                            |
|'http://localhost:3000/customers'           |Customer List                                                                   |
|'http://localhost:3000/customers/new'       |Customer Form                                                                   |
|'http://localhost:3000/salespeople'         |Salespeople List                                                                |
|'http://localhost:3000/salespeople/new'     |Salespeople Form                                                                |
|'http://localhost:3000/sales'               |Sales Record List                                                               |
|'http://localhost:3000/sales/new'           |Sales Record Form filtered only to show form once all needed items are available|
|'http://localhost:3000/sales/history'       |Sale history filtered by salesperson selected                                   |


### PostgreSQL Database
    - Ports: 15432:5432

## Diagram

![img](/images/CarCarDiagram.png)
