from django.urls import path
from .views import (
    api_list_techs,
    api_delete_tech,
    api_list_appointments,
    api_delete_appt,
    api_update_appt_status,
)

urlpatterns = [
    path("technicians/", api_list_techs, name="api_list_techs"),
    path("technicians/<int:pk>/", api_delete_tech, name="api_delete_tech"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_delete_appt, name="api_delete_appts"),
    path(
        "appointments/<int:pk>/<str:appt_status>/",
        api_update_appt_status,
        name="api_update_appt_status",
    ),
]
