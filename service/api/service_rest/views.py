from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment, AutomobileVO
from common.json import ModelEncoder
import json

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vip",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]

    def get_extra_data(self, o):
        return {"technician": o.technician.first_name + " " + o.technician.last_name}

class TechEncoder(ModelEncoder):
    model = Technician
    properties = ["id", "first_name", "last_name", "employee_id", "appointments"]

    def get_extra_data(self, o):
        return {"appointments": o.appointments.count()}


@require_http_methods(["GET", "POST"])
def api_list_techs(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        for tech in techs:
            # appts = Appointment.objects.filter(technician=tech.id)
            print('XXXXXXXXXXXXXXXXXXXXX')
            print(tech.id)
            print(tech.appointments.count())
            print('XXXXXXXXXXXXXXXXXXXXX')
        return JsonResponse({"technicians": techs}, encoder=TechEncoder, safe=False)
    else:
        content = json.loads(request.body)
        employee_id = content["employee_id"]
        if Technician.objects.filter(employee_id=employee_id):
            return JsonResponse({"message": "Employee ID already exists"}, status=400)
        tech = Technician.objects.create(**content)
        return JsonResponse(tech, encoder=TechEncoder, safe=False)


@require_http_methods(["DELETE"])
def api_delete_tech(request, pk):
    try:
        count, _ = Technician.objects.get(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    except Technician.DoesNotExist:
        return JsonResponse({"message": "Invalid Technician id"}, status=400)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request, pk=None):
    if request.method == "GET":
        if pk:
            appts = Appointment.objects.filter(technician=pk)
        else:
            appts = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appts}, encoder=AppointmentEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
            auto = AutomobileVO.objects.filter(vin=content["vin"])
            if auto:
                content["vip"] = True
            else:
                content["vip"] = False
            appointment = Appointment.objects.create(**content)
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid Technician id"}, status=400)


@require_http_methods(["DELETE"])
def api_delete_appt(request, pk):
    try:
        count, _ = Appointment.objects.get(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Invalid Appointment id"}, status=400)


@require_http_methods(["PUT"])
def api_cancel_appt(request, pk):
    try:
        Appointment.objects.filter(id=pk).update(status="canceled")
        appt = Appointment.objects.get(id=pk)
        return JsonResponse(appt, encoder=AppointmentEncoder, safe=False)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Invalid appointment id"}, status=400)


@require_http_methods(["PUT"])
def api_update_appt_status(request, pk, appt_status):
    if appt_status == "cancel":
        appt_status = "Canceled"
    elif appt_status == "finish":
        appt_status = "Complete"
    elif appt_status == "incomplete":
        appt_status = "Incomplete"
    else:
        return JsonResponse({"message": "Invalid appointment status"}, status=400)
    try:
        Appointment.objects.filter(id=pk).update(status=appt_status)
        appt = Appointment.objects.get(id=pk)
        return JsonResponse(appt, encoder=AppointmentEncoder, safe=False)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Invalid appointment id"}, status=400)
