import React, {useState} from 'react';
import { NavLink } from 'react-router-dom';


function CustomerForm() {
    const [formData, setFormData] = useState({
        first_name: "",
        last_name: "",
        address: "",
        phone_number: "",
    })
    const [message, setMessage] = useState(undefined)

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8090/api/customers/';

        const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
            setFormData({
                first_name: "",
                last_name: "",
                address: "",
                phone_number: "",
                });
                setMessage({ type: 'success', success : "Added Customer"})
            } else {
                setMessage({ type: 'error', error : "Cannot add a new customer with the same phone number as another customer"})
            }
        } catch (error) {
            setMessage({ type: 'error', error : "Failed to create"})
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        //Previous form data is spread (i.e. copied) into our new state object
        ...formData,

        //On top of the that data, we add the currently engaged input key and value
        [inputName]: value
        });
        setMessage(undefined)
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Customer to the Database</h1>
                <NavLink to="/customers" className="btn btn-sm btn-success mb-3">Show Customer List</NavLink>
                <form onSubmit={handleSubmit} id="create-model-form">
                    <div className="mb-3">

                        <label htmlFor="first_name">First Name</label>
                        <input
                            onChange={handleFormChange}
                            value={formData.first_name}
                            placeholder="First Name"
                            required name="first_name"
                            id="first_name"
                            className="form-control" />



                    </div>
                    <div className="mb-3">

                        <label htmlFor="last_name">Last Name</label>
                        <input
                            onChange={handleFormChange}
                            value={formData.last_name}
                            placeholder="Last Name"
                            required type="text"
                            name="last_name"
                            id="last_name"
                            className="form-control" />


                    </div>


                    <div className="mb-3">

                        <label htmlFor="address">Address</label>
                        <input
                            onChange={handleFormChange}
                            value={formData.address}
                            placeholder="Address"
                            required type="text"
                            name="address"
                            id="address"
                            className="form-control" />

                    </div>
                    <div className="mb-3">

                        <label htmlFor="phone_number">Phone Number</label>
                        <input
                            onChange={handleFormChange}
                            value={formData.phone_number}
                            placeholder="Phone Number"
                            required type="text"
                            name="phone_number"
                            id="phone_number"
                            className="form-control" />

                    </div>

                    <button className="btn btn-success">Create</button>
                </form>
                <div>
                    {message && (
                        <div className="shadow p-4 mt-4">
                            {message.type === 'success' ? message.success : message.error}
                            <div>
                                <NavLink to="/sales/new" className="btn btn-sm btn-success">Add New Sale Record</NavLink>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
        </div>
    );
}

export default CustomerForm;
