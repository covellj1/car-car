import React, {useState, useEffect} from 'react';
import { NavLink } from 'react-router-dom';


function SaleForm() {
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [automobiles, setAutomobiles]= useState([])
    const [message, setMessage] = useState(undefined)
    const [formData, setFormData] = useState({
        price : "",
        salesperson : "",
        customer : "",
        automobile : "",
    })

    const getSalespeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
        }
    }
    const getCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers);
        }
    }
    const getAutomobiles = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }



    useEffect(() => {
        getSalespeople();
        getCustomers();
        getAutomobiles();
    }, []);
    const handleSubmit = async (event) => {
        event.preventDefault();
        const salesUrl = 'http://localhost:8090/api/sales/';

        const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        try {
            const response = await fetch(salesUrl, fetchConfig);
            if (response.ok) {
                setFormData({
                    price : "",
                    salesperson : "",
                    customer : "",
                    automobile : "",
                });
                setMessage({ type: 'success', success : "Sale Successful"})
                getAutomobiles()
            } else {
                setMessage({ type: 'error', error : "Failed to create sale"})
            }
        } catch (error) {
            setMessage({ type: 'error', error : "Failed to create sale"})
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        //Previous form data is spread (i.e. copied) into our new state object
        ...formData,

        //On top of the that data, we add the currently engaged input key and value
        [inputName]: value
        });
        setMessage(undefined)

    }
    const unsoldAutomobiles = automobiles.filter(automobile => !automobile.sold)
    if (customers.length <= 0) {
        return (
            <div>
                <h1> To make a sale, there needs to be a customer in the database</h1>
                <div>
                    <NavLink to="/customers/new" className="btn btn-sm btn-success mb-3">Add New Customer to Database</NavLink>
                </div>
                <NavLink to="/sales" className="btn btn-sm btn-success mb-3">Show Sales List</NavLink>
            </div>
        )
    } else if (salespeople.length <= 0) {
        return (
            <div>
                <h1> To make a sale, there needs to be a salesperson on the Roster</h1>
                <div>
                    <NavLink to="/salespeople/new" className="btn btn-sm btn-success mb-3">Add New Salesperson to Roster</NavLink>
                </div>
                <NavLink to="/sales" className="btn btn-sm btn-success mb-3">Show Sales List</NavLink>
            </div>
        )
    } else if (unsoldAutomobiles.length <= 0) {
        return (
            <div>
                <h1> To make a sale, there needs to be an automobile available for sale in the inventory</h1>
                <div>
                    <NavLink to="/automobiles/new" className="btn btn-sm btn-success mb-3">Add New Automobile to Inventory</NavLink>
                </div>
                <NavLink to="/sales" className="btn btn-sm btn-success mb-3">Show Sales List</NavLink>
            </div>
        )
    } else {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 className="text-decoration-underline">Create Sale</h1>
                        <NavLink to="/sales" className="btn btn-sm btn-success mb-3">Show Sales List</NavLink>
                        <form onSubmit={handleSubmit} id="create-sale-form">
                            <div className="form-floating mb-3">

                                <input
                                    onChange={handleFormChange}
                                    value={formData.price}
                                    placeholder="Price"
                                    required type="number"
                                    min ="0"
                                    name="price"
                                    id="price"
                                    className="form-control" />
                                <label htmlFor="price">Price</label>

                            </div>

                            <div className="mb-3">

                                <select
                                    onChange={handleFormChange}
                                    value={formData.salesperson}
                                    required name="salesperson"
                                    id="salesperson"
                                    className="form-select">

                                    <option value="">Choose a salesperson</option>
                                    {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.employee_id}>{ salesperson.last_name }, {salesperson.first_name} - ID:{salesperson.employee_id}</option>
                                    )
                                    })}
                                </select>

                            </div>
                            <div className="mb-3">

                                <select
                                    onChange={handleFormChange}
                                    value={formData.customer}
                                    required name="customer"
                                    id="customer"
                                    className="form-select">

                                    <option value="">Choose a Customer</option>
                                    {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>{ customer.first_name } { customer.last_name }: {customer.phone_number}</option>
                                    )
                                    })}
                                </select>

                            </div>
                            <div className="mb-3">

                                <select
                                    onChange={handleFormChange}
                                    value={formData.automobile}
                                    required name="automobile"
                                    id="automobile"
                                    className="form-select">

                                    <option
                                        onChange={handleFormChange}
                                        value="">Choose a Car
                                        </option>
                                    {automobiles.filter(automobile => !automobile.sold).map(automobile => {
                                    return (
                                        <option key={automobile.vin} value={automobile.vin}>Vin number:{automobile.vin}</option>
                                    )
                                    })}
                                </select>

                            </div>
                            <button onChange={handleFormChange} className="btn btn-success">Create</button>
                        </form>
                        <div>
                        {message && (
                            <div className="shadow p-4 mt-4">
                                {message.type === 'success' ? message.success : message.error}
                            </div>
                        )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SaleForm;
