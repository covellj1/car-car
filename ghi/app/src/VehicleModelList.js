import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function VehicleModelList(props) {
    const [vehicleModels, setVehicleModels] = useState([])
    const [deleteModel, setDeleteModel] = useState([])

    const getData = async () => {
        const response= await fetch('http://localhost:8100/api/models')
        if (response.ok) {
            const data = await response.json();
            setVehicleModels(data.models)
        }
    }

    const handleDelete = async (id) =>{
        const url = `http://localhost:8100/api/models/${id}`;
        const fetchConfig= {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
        };
        await fetch(url, fetchConfig);
        getData();
    }

    const deleteConfirmation = async (id) =>{
        setDeleteModel(id);
    }

    useEffect(()=>{
        getData()
    }, [])
    if (vehicleModels.length > 0) {
        return (
            <>
                <div className="container text-center">
                    <h1 className="mb-2 text-decoration-underline">Vehicle Model List</h1>
                    <NavLink to="/models/new" className="btn btn-sm btn-success">Add New Model</NavLink>
                </div>
                <div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Model</th>
                                <th>Picture</th>
                                <th>Manufacturer</th>
                            </tr>
                        </thead>
                        <tbody>
                        {vehicleModels.map(vehicleModel => {
                            return (
                                <tr key={vehicleModel.id}>
                                    <td>{ vehicleModel.name }</td>
                                    <td>
                                        <img
                                            src={ vehicleModel.picture_url }
                                            alt=""
                                            style={{height:150, width:230}}>
                                        </img>
                                    </td>
                                    <td>{ vehicleModel.manufacturer.name }</td>
                                    <td>
                                        <button
                                            type="button"
                                            className="btn btn-outline-danger"
                                            onClick={() => deleteConfirmation(vehicleModel.id)}> Delete Model</button>
                                            {deleteModel === vehicleModel.id && (
                                                <div className="container text-center">
                                                    <h1 className="text-danger">!!WARNING!!</h1>
                                                    <h2>**Deleting this model will delete all the Automobiles in the inventory that are this model**</h2>
                                                    <div>
                                                        <button
                                                            type="button"
                                                            className="btn btn-danger mb-3"
                                                            onClick={() => handleDelete(vehicleModel.id)}>Confirm Delete Model and all associate vehicles
                                                        </button>
                                                    </div>
                                                    <div>
                                                        <button
                                                            type="button"
                                                            className="btn btn-success"
                                                            onClick={() => setDeleteModel(null)}>CANCEL
                                                        </button>
                                                    </div>
                                                </div>
                                            )}
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                </div>
            </>
        );
    } else {
        return (
            <div className="container text-center">
                <h1>No Models in Database. Please add a model.</h1>
                <p>
                    <NavLink to="/models/new" className="btn btn-lg btn-success">Add New Model</NavLink>
                </p>
            </div>
        )
    }
}

export default VehicleModelList;
