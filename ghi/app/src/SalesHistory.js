import { useEffect, useState } from 'react';

function SalesHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState('');
    const [sales, setSales] = useState([]);

    const getSalespeople= async () => {
        const response= await fetch('http://localhost:8090/api/salespeople')
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }

    const getSales= async () => {
        const response= await fetch('http://localhost:8090/api/sales')
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }

    const handleSalespersonChange = (employee_id) =>{
        setSelectedSalesperson(employee_id);
    };

    useEffect(() => {
        getSalespeople();
        getSales();
    }, []);

    return (
        <>
            <div className="container text-center">
                <h1 className="mb-4 text-decoration-underline">Sales history by salesperson</h1>
            </div>
            <div className="dropdown">
                <select className="btn btn-success dropdown-toggle" value={selectedSalesperson} onChange={(e) => handleSalespersonChange(e.target.value)}>
                <option value="">Select Salesperson</option>
                    {salespeople.map(salesperson => (
                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                            {salesperson.last_name}, {salesperson.first_name}-{salesperson.employee_id}
                        </option>
                    ))}
                </select>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Salesperson Name</th>
                            <th>Employee ID</th>
                            <th>Customer Name</th>
                            <th>Automobile VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>

                    {sales.filter(sale => sale.salesperson.employee_id === Number(selectedSalesperson)).map(sale => (
                        <tr key={ sale.id}>
                            <td>{ sale.salesperson.last_name }, { sale.salesperson.first_name }</td>
                            <td>{ sale.salesperson.employee_id }</td>
                            <td>{ sale.customer.last_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>${ sale.price }.00</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default SalesHistory;
