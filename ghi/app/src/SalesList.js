import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';


function SalesList(props) {
    const [sales, setSales] = useState([])

    const getData = async() => {
        const response= await fetch('http://localhost:8090/api/sales/');

        if(response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }

    const handleDelete = async (id) =>{
        const url = `http://localhost:8090/api/sales/${id}`;
        const fetchConfig= {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
        };
        await fetch(url, fetchConfig);
        getData();
    }
    useEffect(()=>{
        getData()
    }, [])

    return (
        <>
            <div className="container text-center">
                <h1 className="text-decoration-underline mb-2">All Sales Records</h1>
                <NavLink to="/sales/new" className="btn btn-sm btn-success">Add New Sale Record</NavLink>
            </div>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Automobile Vin</th>
                            <th>Price</th>
                            <th>Salesperson Name</th>
                            <th>Employee ID</th>
                            <th>Purchaser Name</th>
                            <th>Purchaser Address</th>
                            <th>Purchaser Phone Number</th>
                        </tr>
                    </thead>
                    <tbody>
                    {sales.map(sale => {
                        const key = sale.id;
                        return (
                            <tr key={key}>
                                <td>{ sale.automobile.vin}</td>
                                <td>${ sale.price }.00</td>
                                <td>{ sale.salesperson.last_name }, { sale.salesperson.first_name }</td>
                                <td>{ sale.salesperson.employee_id }</td>
                                <td>{ sale.customer.last_name }, { sale.customer.first_name }</td>
                                <td>{ sale.customer.address }</td>
                                <td>{ sale.customer.phone_number }</td>
                                <td>
                                    <button
                                        type="button"
                                        className="btn btn-outline-danger"
                                        onClick={() => handleDelete(sale.id, props.getsale)}>Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default SalesList;
