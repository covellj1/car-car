import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function SalespersonList(props) {
    const [salespeople, setSalespeople] = useState([])

    const getData = async () => {
        const response= await fetch('http://localhost:8090/api/salespeople')
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }

    const handleDelete = async (id) =>{
        const url = `http://localhost:8090/api/salespeople/${id}`;
        const fetchConfig= {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
        };
        await fetch(url, fetchConfig);
        getData();
    }

    useEffect(()=>{
        getData()
    }, [])

    return (
        <>
            <div className="container text-center">
                <h1 className="text-decoration-underline mb-2">Salesperson Roster</h1>
                <NavLink to="/salespeople/new" className="btn btn-sm btn-success">Add New Salespeople</NavLink>
            </div>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Employee ID</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                    {salespeople.map(salesperson => {
                        const key = salesperson.id;
                        return (
                        <tr key={key}>
                            <td>{ salesperson.employee_id }</td>
                            <td>{ salesperson.last_name }, { salesperson.first_name }</td>
                            <td>
                                <button
                                    type="button"
                                    className="btn btn-outline-danger"
                                    onClick={() => handleDelete(salesperson.id, props.getsalesperson)}>Delete
                                </button>
                            </td>

                        </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default SalespersonList
