import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';


function CustomerList(props) {
    const [customers, setCustomers] = useState([])

    const getData = async () => {
        const response= await fetch('http://localhost:8090/api/customers')
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    const handleDelete = async (id) =>{
        const url = `http://localhost:8090/api/customers/${id}`;
        const fetchConfig= {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
        };
        await fetch(url, fetchConfig);
        getData();
    }

    useEffect(()=>{
        getData()
    }, [])
    return (
        <>
            <div className="container text-center">
                <h1 className="text-decoration-underline mb-2">Customer List</h1>
                <NavLink to="/customers/new" className="btn btn-sm btn-success">Add New Customer to Database</NavLink>
            </div>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                        </tr>
                    </thead>
                    <tbody>
                    {customers.map(customer => {
                        const key = customer.id;
                        return (
                        <tr key={key}>
                            <td>{ customer.last_name }, { customer.first_name }</td>
                            <td>{ customer.address }</td>
                            <td>{ customer.phone_number }</td>
                            <td>
                                <button
                                    type="button"
                                    className="btn btn-outline-danger"
                                    onClick={() => handleDelete(customer.id, props.getcustomer)}>Delete
                                </button>
                            </td>

                        </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default CustomerList
