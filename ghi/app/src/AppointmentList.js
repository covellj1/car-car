import { NavLink } from 'react-router-dom';
import { useState, useEffect } from 'react';

const AppointmentList = () => {
    const [incomplete, setIncomplete] = useState([]);

    async function getAppointments() {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const { appointments } = await response.json();
            const incomplete = [];

            for (const appt of appointments) {
                if (appt.status === 'Incomplete') {
                    incomplete.push(appt);
                }
            }
            setIncomplete(incomplete);

        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        getAppointments();
    }, [])

    const handleUpdate = async (e) => {
        e.preventDefault();
        const appt_id = e.target.value
        const update = e.target.name

        const url = `http://localhost:8080/api/appointments/${appt_id}/${update}/`;
        const fetchConfig = {
            method: "put",
            headers: { 'Content-Type': 'application/json' }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getAppointments();
        }
    };

    return (
        <>
            <div className="row text-center mt-3">
                <h1 className="mb-2">Service Appointments</h1>
                <p><NavLink to="/appointments/new" className="btn btn-sm btn-primary">Schedule New Appointment</NavLink></p>
                <p><NavLink to="/appointments/history" className="btn btn-sm btn-secondary">Service History</NavLink></p>
            </div>
            <div className="row justify-content-center text-center">
                <div className='col'>
                    <table className='table table-striped table-hover'>
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>VIP?</th>
                                <th>Customer</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {incomplete.length > 0 ? incomplete.map(appointment => {
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.vip ? 'Yes' : 'No'}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                                        <td>{new Date(appointment.date_time).toLocaleTimeString(undefined,{hour: "numeric", minute: "numeric"})}</td>
                                        <td>{appointment.technician}</td>
                                        <td>{appointment.reason}</td>
                                        <td>
                                            <button className="btn btn-primary mx-1" onClick={handleUpdate} value={appointment.id} name='finish'>Finish</button>
                                            <button className="btn btn-danger" onClick={handleUpdate} value={appointment.id} name='cancel'>Cancel</button>
                                        </td>
                                    </tr>
                                );
                            }) :
                                <tr>
                                    <td colSpan={'8'}>No Appointments Scheduled :(</td>
                                </tr>
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
}

export default AppointmentList;
