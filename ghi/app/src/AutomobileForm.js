import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
function AutomobileForm() {
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });

    const [models, setModels] = useState([]);

    async function getModels() {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        getModels();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/automobiles/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            });
            navigate("/automobiles")
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    if (models.length > 0) {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add an Automobile to Inventory</h1>
                        <form onSubmit={handleSubmit} id="create-automobile-form">

                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} placeholder="Color..." required type="text" name="color" id="color" className="form-control" value={formData.color} />
                                <label htmlFor="color">Color...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} placeholder="Color..." required type="number" min="1900" max="2099" step="1" name="year" id="year" className="form-control" value={formData.year} />
                                <label htmlFor="year">Year...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} placeholder="VIN..." required type="text" name="vin" id="vin" maxLength="17" className="form-control" value={formData.vin} />
                                <label htmlFor="vin">VIN...</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleFormChange} required name="model_id" id="model_id" className="form-select" value={formData.model_id}>
                                    <option value="">Choose a model...</option>
                                    {models.map(model => {
                                        return (
                                            <option key={model.href} value={model.id}>{model.manufacturer.name} {model.name}</option>
                                        )
                                    })}
                                </select>
                            </div>

                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    } else {
        return (
            <div>
                <h1>Please add a model to create a car</h1>
                <NavLink to="/models/new" className="btn btn-sm btn-success">Add New Model</NavLink>
            </div>
        )
    }
}

export default AutomobileForm;
