import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function AutomobileList(props) {
    const [automobiles, setAutomobiles] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles')
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        }
    }

    const handleDelete = async (id) => {
        const url = `http://localhost:8100/api/automobiles/${id}/`;
        const fetchConfig = {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
        };
        await fetch(url, fetchConfig);
        getData();
    }


    useEffect(() => {
        getData()
    }, [])
    const unsoldAutomobiles = automobiles.filter(automobile => !automobile.sold)
    if (unsoldAutomobiles.length > 0) {
        return (
            <>
                <div className="container text-center">
                    <h1 className="mb-2">Cars available for Sale</h1>
                    <NavLink to="/automobiles/new" className="btn btn-sm btn-success">Add New Automobile to Inventory</NavLink>
                </div>
                <div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Vin Number</th>
                                <th>Color</th>
                                <th>Year</th>
                                <th>Manufacturer</th>
                                <th>Model</th>
                                <th>Sold</th>
                            </tr>
                        </thead>
                        <tbody>
                        {unsoldAutomobiles.map(automobile => {
                            const key = automobile.vin;
                            return (
                            <tr key={key}>
                                <td>{ automobile.vin }</td>
                                <td>{ automobile.color }</td>
                                <td>{ automobile.year }</td>
                                <td>{ automobile.model.manufacturer.name }</td>
                                <td>{ automobile.model.name}</td>
                                <td>{ automobile.sold ? "Yes":"No" }</td>
                                <td>
                                    <button
                                        type="button"
                                        className="btn btn-outline-danger"
                                        onClick={() => handleDelete(automobile.vin, props.getautomobile)}>Delete
                                    </button>
                                </td>
                            </tr>
                            );
                        })}
                        </tbody>
                    </table>
                </div>
            </>
        );
    } else {
        return (
            <div className="container text-center">
                <h1>No Cars Available</h1>
                <p>
                    <NavLink to="/automobiles/new" className="btn btn-sm btn-success">Add New Car</NavLink>
                </p>

            </div>
        )
    }
}

export default AutomobileList;
